package net.stuntguy3000;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandRestrictor extends JavaPlugin implements Listener {
	
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	public String c(String input)
	{
		return ChatColor.translateAlternateColorCodes('&', input);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerCommandPreprocessEvent (PlayerCommandPreprocessEvent event)
	{
		Player p = event.getPlayer();
		
		List<String> cmds = getConfig().getStringList("BlockedCommands");
	    
		for (String command : cmds) {
	        if (event.getMessage().contains("/" + command)) {
	          event.setCancelled(true);
	          p.sendMessage(c("&cYou do not have permission to do that!"));
	        }
	    }

	}
}
